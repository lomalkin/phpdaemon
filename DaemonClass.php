<?php

// Без этой директивы PHP не будет перехватывать сигналы
declare(ticks=1); 
pcntl_signal_dispatch();

class DaemonClass {
	public $maxProcesses = 1;				// Максимальное количество дочерних процессов
	protected $stop_server = FALSE;			// Когда установится в TRUE, демон завершит работу
	protected $currentJobs = array();		// Здесь будем хранить запущенные дочерние процессы

	public function __construct() {
		echo "Сonstructed daemon controller".PHP_EOL;
		// Ждем сигналы SIGTERM и SIGCHLD
		pcntl_signal(SIGTERM, array($this, "childSignalHandler"));
		pcntl_signal(SIGCHLD, array($this, "childSignalHandler"));
	}

	public function run() {
		echo "Running daemon controller".PHP_EOL;

		// Пока $stop_server не установится в TRUE, гоняем бесконечный цикл
		while (!$this->stop_server) {
			// Если уже запущено максимальное количество дочерних процессов, ждем их завершения
			while(count($this->currentJobs) >= $this->maxProcesses) {
				//echo "Maximum children allowed, waiting...".PHP_EOL;
				//sleep(1);
			}

			$this->launchJob();
		} 
	}
	
	protected function daemon_work($pid) {
		echo "Running process with PID {$pid}".PHP_EOL;
		sleep(1);
		exit();
	}
	
	protected function launchJob() {
		// Создаем дочерний процесс весь код после pcntl_fork() будет выполняться двумя процессами: родительским и дочерним
		// pcntl_fork() возвращает -1 в случае возникновения ошибки, $pid будет доступна в родительском процессе,
		// в дочернем этой переменной не будет (точнее она будет равна 0).
		$pid = pcntl_fork();
		if ($pid == -1) {	// Не удалось создать дочерний процесс:
			error_log('Could not launch new job, exiting');
			return FALSE;
		} elseif ($pid) {	// Этот код выполнится родительским процессом:
			$this->currentJobs[$pid] = TRUE;
		} else {	// А этот код выполнится дочерним процессом::
			$this->daemon_work(getmypid());
		} 
		return TRUE; 
	}
	
	public function childSignalHandler($signo, $pid = null, $status = null) {
		switch ($signo) {
			//Мы ожидаем сигналы SIGTERM (завершения работы) и SIGCHLD (от дочерних процессов).
			//Запускаем бесконечный цикл, чтобы демон не завершился.
			//Проверяем, можно ли создать еще дочерний процесс и ждем, если нельзя.
			case SIGTERM:	// При получении сигнала завершения работы устанавливаем флаг:
				$this->stop_server = true;
				break;
			case SIGCHLD:	// При получении сигнала от дочернего процесса:
				if (!$pid) {
					$pid = pcntl_waitpid(-1, $status, WNOHANG); 
				} 
				while ($pid > 0) {	// Пока есть завершенные дочерние процессы:
					if ($pid && isset($this->currentJobs[$pid])) {
						// Удаляем дочерние процессы из списка
						unset($this->currentJobs[$pid]);
					} 
					$pid = pcntl_waitpid(-1, $status, WNOHANG);
				} 
				break;
			default:
				echo "Unknown signal {$signo}".PHP_EOL;
				// все остальные сигналы
		}
	}
} //class

