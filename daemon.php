<?php

// Создаем дочерний процесс
// весь код после pcntl_fork() будет выполняться двумя процессами: родительским и дочерним
$child_pid = pcntl_fork();
if ($child_pid) {	// Выходим из родительского, привязанного к консоли, процесса
	exit();
}
// Делаем основным процессом дочерний.
posix_setsid();

// Дальнейший код выполнится только дочерним процессом, который уже отвязан от консоли
$baseDir = dirname(__FILE__);
ini_set('error_log', $baseDir.'/log_error.log');

//*
fclose(STDIN);
fclose(STDOUT);
fclose(STDERR);

$STDIN = fopen('/dev/null', 'r');
$STDOUT = fopen($baseDir.'/log_application.log', 'ab');
$STDERR = fopen($baseDir.'/log_daemon.log', 'ab');
// */

include 'DaemonClass.php';
$daemon = new DaemonClass();
$daemon->run();
